#!/bin/bash
# script d'allumage des stations Scribe par wakeOnLan
# v. monteil

dpkg -s wakeonlan &> /dev/null
if [ $? -eq 1 ]; then
    echo "Paquet WakeOnLan non installé."
    echo "Installation en cours..."
    apt-get -f install -y wakeonlan
    echo "Installation du Paquet WakeOnLan terminé."
    clear
fi

# fichier par défaut /usr/share/eole/controlevnc/machines.db
dbFilePC="/home/machines.db"
#dbFilePC="/usr/share/eole/controlevnc/machines.db" ## Pour tests
if [ ! -e $dbFilePC ]; then
    echo "Le fichier $dbFilePC n'a pas été trouvé."
    exit
fi

for infopc in `grep ';' $dbFilePC`
do
    nomMachine=$(echo $infopc | awk -F";" '{print $1}')
    ipaddrMachine=$(echo $infopc | awk -F";" '{print $2}')
    madaddrMachine=$(echo $infopc | awk -F";" '{print $4}')
    
    infoIPorMac="$ipaddrMachine - "
    if [ -z "$ipaddrMachine" ]; then
        infoIPorMac=""
    fi
    
    wakeonlan "$madaddrMachine" > /dev/null 2>&1
    echo -e "Machine\t$nomMachine ($infoIPorMac$madaddrMachine) allumée !"
    sleep 1s
done
