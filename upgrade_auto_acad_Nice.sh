#!/bin/bash
clear

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m'
isOK=1

function askMakeSnapshot() {
    echo -e "${RED}"
    read -p "Avez-vous fait un snapshot du Scribe 2.4.2 ? [o/n/m'en fou] : " rep
    echo -e ${NC}
        case $rep in
            y|Y|oui|o)
                echo " "
                echo "C'est bien"
                echo " "
                ;;
            n|N|non)
                echo " "
                echo "Oulala il faut le faire"
                echo " "
                exit
                ;;
            *)
                insult="C'est couillu |T'es pas une lopette|Vient pas pleurer si tas tout pété"
                randNum=$(echo $(( ( RANDOM % 3 )  + 1 )))
                echo "$insult" | cut -d"|" -f $randNum
                echo " "
                ;;
        esac
}

function downloadIso() {
    read -p "Télécharger eole."$2" ? [o/n] (o) : " answer
    : ${answer:=y}
    case $answer in
        y|Y|oui|o)
            mkdir -p /home/iso
            wget http://eole.ac-dijon.fr/pub/iso/EOLE-"$1"/"$2"/eole-"$2"-alternate-amd64.iso --output-document=/home/iso/eole-"$2"-alternate-amd64.iso
            ;;
        *)
            isOK=0
            ;;
    esac
}

function checkMd5Sum(){
    rm -f /tmp/MD5SUMS*
    wget -q http://eole.ac-dijon.fr/pub/iso/EOLE-"$1"/"$2"/MD5SUMS --output-document=/tmp/MD5SUMS
    cat /tmp/MD5SUMS | grep eole-"$2"-alternate-amd64.iso > /tmp/MD5SUMS_"$2"
    sed -i 's/\*/\*\/home\/iso\//g' /tmp/MD5SUMS_"$2"
    md5sum -c /tmp/MD5SUMS_"$2"
    if (( $? ))
    then
        echo -e "${RED}L'empreinte md5 du fichier eole-"$2"-alternate-amd64.iso n'est pas correcte.${NC}"
        downloadIso $1 $2
    else
        echo -e "${GREEN}L'empreinte md5  du fichier eole-"$2"-alternate-amd64.iso est correct.${NC}"
    fi
}

function checkIsoOrDownload () {
    echo -e "${BLUE}Vérification de la présence de l'iso /home/iso/eole-"$2"-alternate-amd64.iso ...${NC}"
    if [ -f /home/iso/eole-"$2"-alternate-amd64.iso ]
    then
        echo -e "${GREEN}/home/iso/eole-"$2"-alternate-amd64.iso présent${NC}"
        echo -e "${BLUE}Vérification de l'empreinte md5 ...${NC}"
    checkMd5Sum $1 $2
    else
        echo -e "${RED}/home/iso/eole-"$2"-alternate-amd64.iso absent${NC}"
        downloadIso $1 $2
    if [ $isOK -eq 1 ]; then
        checkMd5Sum $1 $2
    else
        isOK=1
    fi
fi
}

# Version du Scribe
scribeVersion=$(CreoleGet eole_version)
scribeOSVersion=$(lsb_release -r | awk -F " " '{print $2}')

if [ "$scribeVersion" == "2.4" ]; then
    askMakeSnapshot
fi

if [ "$scribeVersion" != "2.6" ] || [ "$scribeOSVersion" != "16.04" ]; then
    #checkIsoOrDownload majeure mineure
    checkIsoOrDownload "2.5" "2.5.2.4"
    checkIsoOrDownload "2.6" "2.6.2.1"
fi

#on fait de la place dans /var
#del base bacula
rm -rf /var/lib/bacula/*
#nettoyage log
rm -rf /var/log/ead/ead-web.log.*
rm -rf /var/log/controle-vnc/main.log.*
echo '  ' > /var/log/creoled.log

#problème des paquets non authentifiés
wget http://eole.ac-dijon.fr/eole/pool/main/e/eole-keyring/eole-archive-keyring_2018.05.30-1_all.deb
dpkg -i eole-archive-keyring_2018.05.30-1_all.deb
apt-key list

# Mise à jour
mv -f /etc/udev/rules.d/70-persistent-net.rules /root/
rm -f /home/majacad/2000-01-01-1-maintenance
rsync -rvz rsync://apollon.ac-nice.fr/eole26/2.6.2/scribe/majacad/fichiers/post-install-2.6.2.sh /root/.


# Redimensionnement de l'espace disque root
rootSize=$(df -m | awk '/\/$/ {print $2}')
if [[ $rootSize -lt 10240 ]]; then
    echo "Redimensionnement de l'espace disque Racine"
    vgFree=$(vgdisplay --unit M | awk '/Free/ {print $7}' | cut -d "," -f1)
    if [[ $(( $rootSize + $vgFree )) -ge 10240 ]]; then
        lvextend --size 10G /dev/mapper/scribe--vg-root
        resize2fs /dev/mapper/scribe--vg-root
    else
        lvextend --extents +100%FREE /dev/mapper/scribe--vg-root
        resize2fs /dev/mapper/scribe--vg-root
    fi
    echo "Espace disque Racine redimensionné"
fi

# Vérification de l'espace disponible pour la mise à niveau
rootFree=$(df -m | awk '/\/$/ {print $4}')
if [[ $rootFree -le 3072 ]]; then
    echo " "
    echo -e "${RED}Pas assez d'espace disponible pour effectuer la migration.${NC}"
    echo -e "${RED}Le script va s'arrêter.${NC}"
    echo " "
    exit
fi


# upgrade
if [ "$scribeVersion" == "2.4" ]; then
    echo " "
    crontab -u root -l | grep -vE "PATH|nmbd|nmblookup|rsync|run" | crontab -u root -
    service exim4 stop
    rm -Rf /var/spool/exim4/
    mkdir -p /var/spool/exim4/input
    mkdir -p /var/spool/exim4/msglog
    mkdir -p /var/spool/exim4/db
    killall exim
    killall exim4
    chmod 400 /etc/init.d/exim4
    cp -f /usr/share/eole/controlevnc/machines.db /home/
    echo " "
    echo " "
    echo -e "${GREEN}Relancer l'enregistrement zephir, afin de modifier${NC}"
    echo -e "${GREEN}l'adresse zephir (194.167.84.103) par zephir2.ac-nice.fr${NC}"
    echo -e "${GREEN}choix --> 1 - Ne rien faire${NC}"
    echo " "
    echo " "
    enregistrement_zephir
    echo " "
    echo -e "${GREEN}Passage en Scribe 2.5${NC}"
    echo -e "${RED}!!!! ATTENTION migrer la conf sur zephir (vers 2.5.2 variante acad_std) !!!${NC}"
    echo " "
    Maj-Auto
    reconfigure
    apt-get update
    Upgrade-Auto  --force --release 2.5.2 --iso /home/iso/eole-2.5.2.4-alternate-amd64.iso
    touch /root/scribeUpgraded2425
    echo " "
    echo -e "${GREEN}Changement du mot de passe root : \$eole&123456\$${NC}"
    echo "root:\$eole&123456\$" | /usr/sbin/chpasswd
    echo " "
    echo -e "${GREEN}-Redémarrer${NC}"
    echo -e "${GREEN}-Ré-exécuter ce script après le redémarrage final.${NC}"
    echo " "
    
elif [ "$scribeVersion" == "2.5" ]; then
    if [ -e /root/scribeUpgraded2425 ]; then
        ouvre.firewall
        instance
        reconfigure
        echo " "
        echo -e "${GREEN}-Ré-exécuter ce script après le redémarrage.${NC}"
        echo " "
        rm -f /root/scribeUpgraded2425
        reboot
        exit
    fi
    
    echo " "
    crontab -u root -l | grep -vE "PATH|nmbd|nmblookup|rsync|run" | crontab -u root -
    service exim4 stop
    rm -Rf /var/spool/exim4/
    mkdir -p /var/spool/exim4/input
    mkdir -p /var/spool/exim4/msglog
    mkdir -p /var/spool/exim4/db
    killall exim
    killall exim4
    chmod 400 /etc/init.d/exim4
    ouvre.firewall
    Maj-Auto
    reconfigure
    apt-get update
    apt-get remove -y eole-ajaxplorer
    apt-get remove -y eole-bacula
    apt-get autoremove -y
    echo " "
    echo -e "${RED}!!!! ATTENTION migrer la conf sur zephir (vers 2.6.2 variante acad_std) !!!${NC}"
    echo -e "${GREEN}Passage en Scribe 2.6${NC}"
    echo " "
    Upgrade-Auto  --force --release 2.6.2 --iso /home/iso/eole-2.6.2.1-alternate-amd64.iso
    chmod 775 /etc/init.d/exim4
    mv /etc/init.d/networking /etc/init.d/networking.old
    mv /etc/init.d/networking.dpkg-dist /etc/init.d/networking
    systemctl mask gpm
    touch /root/scribeUpgraded2526
    echo " "
    echo -e "${GREEN}-Redémarrer${NC}"
    echo -e "${GREEN}-Ré-exécuter ce script après le redémarrage.${NC}"
    echo " "

elif [ "$scribeVersion" == "2.6" ] || [ "$scribeOSVersion" == "16.04" ]; then
    if [ ! -e /root/scribeUpgraded2526 ]; then
        echo -e "${RED}Le serveur Scribe a déjà été migré en 2.6.2. Le script va s'arrêter.${NC}"
        exit
    fi
    
    if [ -e /root/reboot26PostInstall ]; then
        ouvre.firewall
        /root/post-install-2.6.2.sh
        reconfigure
        rm -f /root/scribeUpgraded2526 /root/reboot26PostInstall
        reboot
        exit
    fi
    
    crontab -u root -l | grep -vE "PATH|nmbd|nmblookup|rsync|run" | crontab -u root -
    echo " "
    echo -e "${GREEN}L'enregistrement zephir va se relancer afin de redescendre${NC}"
    echo -e "${GREEN}la configuration du serveur en 2.6.2${NC}"
    echo -e "${GREEN}Lorsqu'il est demandé, ré-établir à nouveau une configuration réseau minimale (Oui) AVEC le DNS renseigné${NC}"
    echo -e "${GREEN}choix --> 2 - Relancer l'enregistrement${NC}"
    echo " "
    ouvre.firewall
    enregistrement_zephir --force
    rsync -rvz rsync://apollon.ac-nice.fr/eole26/2.6.2/scribe/majacad/fichiers/post-install-2.6.2.sh /root/.
    instance
    if [ ! -e /root/reboot26PostInstall ]; then
        touch /root/reboot26PostInstall
        
        echo -e "${GREEN}-Redémarrer${NC}"
        echo -e "${GREEN}-Ré-exécuter ce script après le redémarrage.${NC}"
        echo -e "${GREEN}qui lancera automatiquement le script /root/post-install-2.6.2.sh${NC}"
        echo " "
    fi

else
    echo -e "${RED}Version scribe non reconnue. Quit${NC}"
    exit
fi

sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*/GRUB_CMDLINE_LINUX_DEFAULT=\"\"/g' /etc/default/grub
update-grub
