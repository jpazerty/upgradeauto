#!/bin/bash

###########################
## rsync tftpboot + recons
###########################

find /home/majacad/ -type f -name "*~*" -exec rm -f {} \;

sleep $(( $RANDOM % 3600))

if [ ! -e /home/majacad/flags/no_maintenance ] ; then

	mkdir /home/majacad/flags > /dev/null 2>&1
	echo "rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/tftpboot/* + rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/recons/* patience ..."
	rsync -rvz rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/tftpboot/* /var/lib/tftpboot/  > /dev/null 2>&1
	rsync -rvz rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/recons/* /home/r/recons/perso/  > /dev/null 2>&1

	ghost=$(CreoleGet numero_etab | cut -c-3)

		if [ "$ghost" == 006 ] ; then
			rsync -rvz rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/majacad/fichiers/ghost/CG06/* /home/r/recons/perso/ghost/.  > /dev/null 2>&1
		fi

		if [ "$ghost" == 083 ] ; then
			rsync -rvz rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/majacad/fichiers/ghost/CG83/* /home/r/recons/perso/ghost/.  > /dev/null 2>&1
		fi
	echo "Fin rsync !"
fi
chown recons.professeurs /home/r/recons/perso -R
chmod -R 775 /home/r/recons


################################################
## reinitialiser  compte et  mdp PAM si chang�
################################################

/usr/share/eole/backend/creation-prof.py -u pam -c 0 -G professeurs,DomainAdmins,DomainUsers -M local -z 0 -q 0 -P 1 -m 'new_bo$$' -p pam -f compte_rectorat -x 1

	if  [ "$?" == 0 ] ; then
		rm -Rf /home/p/pam/perso
		cd /home/p/pam
		[ ! -L perso ] && ln -s /home/r/recons/perso /home/p/pam/perso
	fi

#changement mdp pam
userp=pam ; passp='new_bo$$' ; expect -c "set timeout 5" -c "spawn smbldap-passwd $userp" -c "expect \"New password:\"" -c "send \"$passp\r\""  -c "expect \"Retype new password:\"" -c "send \"$passp\r\"" -c "interact"



#####################################################
## reinitialiser  compte et  mdp GESTSALLE si chang�
#####################################################

/usr/sbin/smbldap-userdel gestsalle
/usr/share/eole/backend/creation-prof.py -u gestsalle -c 0 -G professeurs,DomainUsers -M local -z 0 -q 0 -P 1 -m gestsalle -p gestsalle -f compte_gestion_image -x 1

	if  [ "$?" == 0 ] ; then
		cd /home/g/gestsalle/perso
		[ ! -L Win7 ] && ln -s /home/r/recons/perso/Win7 Win7
		[ ! -L WinXP ] && ln -s /home/r/recons/perso/WinXP WinXP
		[ ! -L station.typ ] && ln -s /home/r/recons/perso/station.typ station.typ
		[ ! -L outils ] && ln -s /home/r/recons/perso/outils outils
		[ ! -L ghost ] && ln -s /home/r/recons/perso/ghost ghost
	fi

#changement mdp gestsalle
userg=gestsalle ; passg='gestsalle' ; expect -c "set timeout 5" -c "spawn smbldap-passwd $userg" -c "expect \"New password:\"" -c "send \"$passg\r\""  -c "expect \"Retype new password:\"" -c "send \"$passg\r\"" -c "interact"


#######################################################
### Regeneration du compte domaine avec pass par d�faut
########################################################

/usr/sbin/smbldap-userdel domaine
/usr/share/eole/backend/creation-prof.py -u domaine -c 0 -G professeurs,DomainAdmins,DomainUsers -M local -z 0 -q 0 -P 1 -m 'a5c90e33fb601d58aad3b435b51404ee989d78a2c1deca1b247b47a7f8b689ad' -p domaine -f compte_rectorat -x 1
smbldap-usermod -A 1 -s /bin/false -g "DomainAdmins" domaine
userd=domaine ; passd='a5c90e33fb601d58aad3b435b51404ee989d78a2c1deca1b247b47a7f8b689ad' ; expect -c "set timeout 5" -c "spawn smbldap-passwd $userd" -c "expect \"New password:\"" -c "send \"$passd\r\""  -c "expect \"Retype new password:\"" -c "send \"$passd\r\"" -c "interact"


#########################
##        acl
#########################

setfacl -R -b /home/r/recons
setfacl -R -m user:recons:rwx /home/r/recons
setfacl -R -m user:pam:rwx /home/r/recons
setfacl -R -m user:gestsalle:rx /home/r/recons
setfacl -R -m default:user:recons:rwx /home/r/recons
setfacl -R -m default:user:pam:rwx /home/r/recons
setfacl -R -m default:user:gestsalle:rx /home/r/recons

#setfacl -R -m u:pam:rwx /home/r/recons
#setfacl -R -m d:u:pam:rwx /home/r/recons

#setfacl -R -m u:gestsalle:rx /home/r/recons
#setfacl -R -m d:u:gestsalle:rx /home/r/recons



#################################
##  purge /tmp (transfert Pidio)
#################################

rm -Rf /tmp/*_tmpZipDir



##############################################################################################################################
### EcoStations- l'utilisateur peut programmer l'extinction des postes pour un jour, sinon on force � H1=20:50 et H2=23:30 ###
##############################################################################################################################

sed -i '/ecoStations/d' /var/spool/cron/crontabs/root
rep=/var/www/html/outils/ecoStations/config/
if [[ -f "$rep"ecoStations.txt ]];then
    sed -i '2s/.*/20\:50/' "$rep"ecoStations.txt
    sed -i '3s/.*/23\:30/' "$rep"ecoStations.txt
    sed -i "2s/.*/\$horaire1\=\'20\:50\'\;/" "$rep"confEcoStations.inc.php
    sed -i "3s/.*/\$horaire2\=\'23\:30\'\;/" "$rep"confEcoStations.inc.php
else
    cat >"$rep"ecoStations.txt <<EOL
[horaires]
20:50
23:30

[horaire1_exclusions]

[horaire2_exclusions]
EOL

    cat >"$rep"confEcoStations.inc.php <<EOL
<?php
\$horaire1='20:50';
\$horaire2='23:30';
\$etat_plan_stop='on';
?>
EOL
fi
chown root:www-data "$rep"confEcoStations.inc.php
chown root:www-data "$rep"ecoStations.txt
setfacl -m g:www-data:rw "$rep"ecoStations.txt
setfacl -m g:www-data:rw "$rep"confEcoStations.inc.php
perl /var/www/html/outils/ecoStations/suppr_cron_stop.pl
perl /var/www/html/outils/ecoStations/IecoStations.pl

#######################################################
### Homedata - derniere version auth cas envole sso ###
#######################################################

#rsync -rvz rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/majacad/fichiers/homedata/* /etc/cron.daily/
rm -rf /etc/cron.daily/homedata >/dev/null 2>&1


#########################
### Relance services  ###
#########################
#crontab -l | grep -v Test | crontab -
#crontab -l | grep -v PATH | crontab -
#crontab -l | grep -v nmblookup | crontab -

crontab -l | grep 'CreoleGet smb_workgroup' >/dev/null 2>&1
if [ "$?" -ne 0 ];then
    crontab -l | { cat; echo '# Test service nmbd'; } | crontab -
    crontab -l | { cat; echo 'PATH=/usr/share/eole:/usr/share/eole/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/share/eole/sbin'; } | crontab -
    crontab -l | { cat; echo '* 7-20 * * * CreoleRun "nmblookup -U 127.0.0.1 $(CreoleGet smb_workgroup) |grep -q '\''name_query failed to find name '\''" fichier; if [ "$?" -ne 1 ]; then service nmbd restart >/dev/null 2>&1 ; fi'; } | crontab -
fi




#############################################################################################################################################
### Traitement sauts de ligne et lignes vides /netlogon/scripts/*/*/* +transfert.bat +application data +ajout Qlaunch-config_eole !!! ??? ###
#############################################################################################################################################

# Qlaunch modifi� pour config_eole
rsync -rvz rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/majacad/fichiers/quicklaunch_XP/* /home/netlogon/scripts/groups/

find /home/netlogon/scripts/ -name 'copie_application_data_profil.bat' -type f -exec rm -f {} \;
find /home/netlogon/scripts/ -name 'transfert.bat' -type f -exec rm -f {} \;
find /home/netlogon/scripts/ -name '*.txt' -type f -exec sed -i -e '/copie_application_data_profil.bat/d' -- {} +
find /home/netlogon/scripts/ -name '*.txt' -type f -exec sed -i -e '/transfert.bat/d' -- {} +
find /home/netlogon/scripts/ -name '*.txt' -type f -exec sed -i 's/^M//g' -- {} +
find /home/netlogon/scripts/ -name '*.txt' -type f -exec sed -i -e 's/HIDDEN/HIDDEN\n/g' -- {} +
find /home/netlogon/scripts/ -name '*.txt' -type f -exec sed -i -e '/^[ \t]*$/d' -- {} +

######################################################################
### Nettoyage Maj-Auto, anciens noyaux linux saturant l'espace vg-root
######################################################################

echo $(dpkg --list | grep linux-image | awk '{ print $2 }' | sort -V | sed -n '/'`uname -r`'/q;p') $(dpkg --list | grep linux-headers | awk '{ print $2 }' | sort -V | sed -n '/'"$(uname -r | sed "s/\([0-9.-]*\)-\([^0-9]\+\)/\1/")"'/q;p') | xargs sudo apt-get -y purge

#############################################################################################################################
### Configuration .muttrc + bacula-dir.conf pour ent�tes emails norm�es root|bacula@eolesso_adresse pour email root et bacula
#############################################################################################################################

rsync -rvz rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/majacad/fichiers/root/* /root/
RNE=`CreoleGet numero_etab`
LIBELLE=`CreoleGet libelle_etab`
DNSSO=`CreoleGet eolesso_adresse`
MAILCMD=`echo "mailcommand = \"/usr/bin/mutt -e \\\\\\\\\"send_charset=utf-8\\\\\\\\\" -s \\\\\\'%e: \\\\\\\\\"%n\\\\\\\\\" $RNE bacula scribe de $LIBELLE \\\\\\' \-\- %r"\"`
OPCMD=`echo "operatorcommand = \"/usr/bin/mutt -e \\\\\\\\\"send_charset=utf-8\\\\\\\\\" -s \\\\\\'%e: \\\\\\\\\"%n\\\\\\\\\" $RNE bacula scribe de $LIBELLE \\\\\\' \-\- %r"\"`
rm /etc/bacula/bacula-dir.conf.sav > /dev/null 2>&1
sed -i "s/REALNAME/root@$DNSSO/g" /root/.muttrc
sed -i "s/@DNSSO/@$DNSSO/g" /root/.muttrc
sed -i '/mailcommand/{c\'"  $MAILCMD"''$'\n'';d}' /etc/bacula/bacula-dir.conf
sed -i '/operatorcommand/{c\'"  $OPCMD"''$'\n'';d}' /etc/bacula/bacula-dir.conf
cp -f /root/.muttrc /var/lib/bacula/
sed -i 's/root@/bacula@/g' /var/lib/bacula/.muttrc
chown bacula /var/lib/bacula/.muttrc

#Exclusions
sed -i '/station.typ/d' /etc/bacula/bacula-dir.conf
sed -i '/stations.typ/d' /etc/bacula/bacula-dir.conf
#sed -i '/Exclude/a\\tfile = /home/r/recons/perso/station.typ\t' /etc/bacula/bacula-dir.conf
sed -i '/gestsalle/d' /etc/bacula/bacula-dir.conf
#sed -i '/Exclude/a\\tfile = /home/g/gestsalle\t' /etc/bacula/bacula-dir.conf
sed -i '/\/home\/p\/pam/d' /etc/bacula/bacula-dir.conf
#sed -i '/Exclude/a\\tfile = /home/p/pam\t' /etc/bacula/bacula-dir.conf
fic="/etc/bacula/baculafichiers.d/fichier.conf"
sed -i '/\/perso\/\.Config/d' "$fic"
sed -i '/\/perso\/config_eole/d' "$fic"
sed -i '/\/home\/p\/pam/d' "$fic"
sed -i '/\/home\/g\/gestsalle/d' "$fic"
sed -i '/\perso\/station\.typ/d' "$fic"

sed -i '/exclude\ =\ yes/i \
    wilddir\ =\ \"\*\/perso\/\.Config\" \
    wilddir\ =\ \"\*\/perso\/config_eole\"' "$fic"

sed -i '/Exclude\ {/a \
  File\ =\ \/home\/p\/pam \
  File\ =\ \/home\/g\/gestsalle \
  File\ =\ \/home\/r\/recons\/perso\/station\.typ' "$fic"
# Correction durees de retention
sed -i -e "s/6 months/2 weeks/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/1 months/2 weeks/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/10 days/21 days/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/14 days/21 days/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/5 weeks/2 weeks/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/1 weeks/2 weeks/g" /etc/bacula/bacula-dir.conf

if [ ! -e /usr/share/eole/postservice/99-baculapost ] ; then
echo "Ajout postservice 99-baculapost..."
cat > /usr/share/eole/postservice/99-baculapost <<'EOF'
#!/bin/bash
## MUTTRC BACULA

RNE=`CreoleGet numero_etab`
LIBELLE=`CreoleGet libelle_etab`
DNSSO=`CreoleGet eolesso_adresse`

MAILCMD=`echo "mailcommand = \"/usr/bin/mutt -e \\\\\\\\\"send_charset=utf-8\\\\\\\\\" -s \\\\\\'%e: \\\\\\\\\"%n\\\\\\\\\" $RNE bacula scribe de $LIBELLE \\\\\\' \-\- %r"\"`
OPCMD=`echo "operatorcommand = \"/usr/bin/mutt -e \\\\\\\\\"send_charset=utf-8\\\\\\\\\" -s \\\\\\'%e: \\\\\\\\\"%n\\\\\\\\\" $RNE bacula scribe de $LIBELLE \\\\\\' \-\- %r"\"`

sed -i '/mailcommand/{c\'"  $MAILCMD"''$'\n'';d}' /etc/bacula/bacula-dir.conf
sed -i '/operatorcommand/{c\'"  $OPCMD"''$'\n'';d}' /etc/bacula/bacula-dir.conf
cp -f /root/.muttrc /var/lib/bacula/
sed -i 's/root@/bacula@/g' /var/lib/bacula/.muttrc
chown bacula /var/lib/bacula/.muttrc

#Exclusions
sed -i '/station.typ/d' /etc/bacula/bacula-dir.conf
sed -i '/stations.typ/d' /etc/bacula/bacula-dir.conf
#sed -i '/Exclude/a\\tfile = /home/r/recons/perso/station.typ\t' /etc/bacula/bacula-dir.conf
sed -i '/gestsalle/d' /etc/bacula/bacula-dir.conf
#sed -i '/Exclude/a\\tfile = /home/g/gestsalle\t' /etc/bacula/bacula-dir.conf
sed -i '/\/home\/p\/pam/d' /etc/bacula/bacula-dir.conf
#sed -i '/Exclude/a\\tfile = /home/p/pam\t' /etc/bacula/bacula-dir.conf
sed -i '/exclude\ =\ yes/i \
    wilddir\ =\ \"\*\/perso\/\.Config\" \
    wilddir\ =\ \"\*\/perso\/config_eole\"' "$fic"

sed -i '/Exclude\ {/a \
  File\ =\ \/home\/p\/pam \
  File\ =\ \/home\/g\/gestsalle \
  File\ =\ \/home\/r\/recons\/perso\/station\.typ' "$fic"
# Correction durees de retention
sed -i -e "s/6 months/2 weeks/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/1 months/2 weeks/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/10 days/21 days/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/14 days/21 days/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/5 weeks/2 weeks/g" /etc/bacula/bacula-dir.conf
sed -i -e "s/1 weeks/2 weeks/g" /etc/bacula/bacula-dir.conf

exit 0
EOF
chmod +x /usr/share/eole/postservice/99-baculapost
else echo "postservice 99-baculapost Ok"
fi
service bacula-director reload  > /dev/null 2>&1

################################################################
### Preparation WPKG - packages - manager - software installers
################################################################

rsync -rvz rsync://apollon.ac-nice.fr/eole24/2.4.2/scribe/majacad/fichiers/wpkg/* /home/wpkg/  > /dev/null 2>&1
cd /root
if [ ! -e /root/wpkg-manage.zip ] ; then
rm -rf /home/wpkg/wpkg-manage/
wget http://eole.ac-dijon.fr/pub/Outils/Wpkg-manage/wpkg-manage.zip
/usr/bin/unzip wpkg-manage.zip  > /dev/null 2>&1
mv wpkg-manage /home/wpkg/
else echo "wpkg-manage eole OK"
fi
apt-eole install git-core curl
cd /root
git clone https://dev-eole.ac-dijon.fr/git/wpkg-package
cd /root/wpkg-package
git pull
cd /root/wpkg-package
rsync -Cav . /home/wpkg  > /dev/null 2>&1
cd /home/wpkg/packages/
nohup ./download_installers.py > /dev/null 2>&1 &
ln -s /home/wpkg /home/a/admin/perso/wpkg
rm -f /home/wpkg/wpkg

######################################################
### suppression des liens morts dans /home/classes ###
######################################################

find /home/classes -type l | perl -lne 'print if ! -e' | xargs rm -rf

###########################################
### Sp�cial var ###########################
###########################################
dept=$(echo $(CreoleGet numero_etab) | head -c 3)
if [ "$dept" -eq 83 ]; then
    # changement mdp recons
    userp="recons" ; passp='recons!83' ; expect -c "set timeout 5" -c "spawn smbldap-passwd $userp" -c "expect \"New password:\"" -c "send \"$passp\r\""  -c "expect \"Retype new password:\"" -c "send \"$passp\r\"" -c "interact"
    # changement mdp admin
    userp="admin" ; passp='bab06_83' ; expect -c "set timeout 5" -c "spawn smbldap-passwd $userp" -c "expect \"New password:\"" -c "send \"$passp\r\""  -c "expect \"Retype new password:\"" -c "send \"$passp\r\"" -c "interact"
    # changement mdp admin.profil
    userp="admin.profil" ; passp='ATIC!83' ; expect -c "set timeout 5" -c "spawn smbldap-passwd $userp" -c "expect \"New password:\"" -c "send \"$passp\r\""  -c "expect \"Retype new password:\"" -c "send \"$passp\r\"" -c "interact"

    #mdp eole
    echo "eole:acadnice_83" | /usr/sbin/chpasswd

    #suppression des DomainAdmins sauf pour admin|admin.profil|pam|recons|domaine|ghost_console|ghost.console
    smbldap-groupshow DomainAdmins | grep memberUid | awk '{print $2}' | sed 's/\,/\\\n/g' | sed 's/\\//g' | grep -vE "^admin$|admin.profil|^pam$|recons|domaine|ghost_console|ghost.console" | xargs -I{} smbldap-groupmod -x {} DomainAdmins
    smbldap-groupmod -m ghost.console DomainAdmins

    # lister les utilisateurs (sauf admin) avec le rôle administrateur du scribe
    #ldapsearch -x -LLL -b "ou=local,ou=personnels,ou=utilisateurs,ou=$(CreoleGet numero_etab),ou=ac-nice,ou=education,o=gouv,c=fr" uid | grep uid: | awk '{print $2}'| grep -vE "^admin$" |  xargs -I {} grep {} /usr/share/ead2/backend/config/roles_local.ini | rev | grep -E "^nimda\ =\ " | rev | sort -u

    # supprimer les utilisateurs (sauf admin) avec le rôle administrateur du scribe
    #ldapsearch -x -LLL -b "ou=local,ou=personnels,ou=utilisateurs,ou=$(CreoleGet numero_etab),ou=ac-nice,ou=education,o=gouv,c=fr" uid | grep uid: | awk '{print $2}'| grep -vE "^admin$" |  xargs -I {} grep {} /usr/share/ead2/backend/config/roles_local.ini | rev | grep -E "^nimda\ =\ " | rev | sort -u | xargs -I {} sed -i '/{}/d' /usr/share/ead2/backend/config/roles_local.ini; service ead-server restart

    # lister les utilisateurs (sauf admin) avec le rôle administrateur du scribe et synchroniser dossier importation
    result=($(ldapsearch -x -LLL -b "ou=local,ou=personnels,ou=utilisateurs,ou=$(CreoleGet numero_etab),ou=ac-nice,ou=education,o=gouv,c=fr" uid | grep uid: | awk '{print $2}'| grep -vE "^admin$" |  xargs -I {} grep {} /usr/share/ead2/backend/config/roles_local.ini | rev | grep -E "^nimda\ =\ " | rev | awk '{print $1}' | sort -u))
    for name in "${result[@]}"; do
        letter=$(echo ${name:0:1})
        chemin=/home/$letter/$name/perso/importation
        mkdir -p $chemin > /dev/null 2>&1
        rsync --ignore-existing /home/a/admin/perso/importation/* $chemin
    done
fi

##################################################


exit 0



