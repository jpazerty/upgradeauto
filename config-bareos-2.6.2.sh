#!/bin/bash
# script de configuration de Bareos - Scribe 2.6.2
# v. monteil

departement=$(CreoleGet numero_etab | head -c 3)
if [ ${departement} == "006" ]; then
    dpt="6"
elif [ ${departement} == "083" ]; then
    dpt="83"
fi
echo "Paramétrage des sauvegardes Bareos..."
read -p "Entrez le numéro d'établissement (10.{6/83}.XXX.85) : " numEtab
read -p "Entrez le nom de la PAM en charge du serveur (cannes/nice/toulon) : " mailDiagPAM
mailDiagPAM=$(echo ${mailDiagPAM} | tr [:upper:] [:lower:])

if [ -z ${numEtab} ]; then echo "Numéro établissement non renseigné."; exit; fi
if [ -z ${mailDiagPAM} ]; then echo "PAM non renseignée."; exit; fi
/usr/share/eole/sbin/bareosconfig.py -s smb --smb_machine="10.${dpt}.${numEtab}.85" --smb_ip="10.${dpt}.${numEtab}.85" --smb_partage="pedago" --smb_login="sauve" --smb_password="nw4sp9"
/usr/share/eole/sbin/bareosconfig.py -x 4
/usr/share/eole/sbin/bareosconfig.py -x 3
/usr/share/eole/sbin/bareosconfig.py -x 2
/usr/share/eole/sbin/bareosconfig.py -x 1
/usr/share/eole/sbin/bareosconfig.py -j monthly --job_level=Full --job_day=7 --job_hour=20
/usr/share/eole/sbin/bareosconfig.py -j weekly --job_level=Differential --job_day=6 --job_hour=23
/usr/share/eole/sbin/bareosconfig.py -j daily --job_level=Incremental --job_day=2 --job_end_day=5 --job_hour=23
/usr/share/eole/sbin/bareosconfig.py -m --mail_ok="" --mail_error="diag.pam-${mailDiagPAM}@ac-nice.fr"
sed -i -e 's/bacula/bareos/g' /etc/profile.d/eolerc.sh

echo "Suppression des anciennes sauvegardes de Bacula 2.4.2"
/usr/share/eole/sbin/bareosmount.py -t > /dev/null 2>&1
if [ $? -eq 0 ]; then
    /usr/share/eole/sbin/bareosmount.py --mount > /dev/null 2>&1
    rm -rf /mnt/sauvegardes/*
    /usr/share/eole/sbin/bareosmount.py --umount > /dev/null 2>&1
else
    echo "Erreur lors du montage du support des sauvegardes"
    exit
fi

echo ""
echo "#######################################################"
echo "### Affichage du paramétrage des sauvegardes Bareos ###"
echo "#######################################################"
/usr/share/eole/sbin/bareosconfig.py -d
echo "Paramétrage des sauvegardes Bareos terminée !"

exit
